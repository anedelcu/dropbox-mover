
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.codec.binary.Base64;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

class cs275_lab1 {
	
	private static String temboo_account;
	private static String temboo_app_name;
	private static String temboo_app_key;
	private static String temboo_app_credential;
	private static String dropbox_list_file = "/move/__list.txt";
	private static String app_src_dir = "/move/" ;
	
	private static boolean error = false;
	private static String errorString="";
	
	public static void main (String argv[]) {
		
		// main will handle the steps the program should take in order to move files
		// from Dropbox to the local computer. It puts together all the other functions
		// that handle reading, writing, deleting Dropbox files as well as writing on the
		// local computer
		
		// reading the input parameters, that overwrite the credentials specified in this file
		if(argv.length == 4) {
			temboo_account = argv[0];
			temboo_app_name = argv[1];
			temboo_app_key = argv[2];
			temboo_app_credential = argv[3];
		} else {
			error = true;
			errorString = "[ ERROR ]  There need to be exactly 4 arguments.";
			showError(true);
		}
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session=null;
		try {
			session = new TembooSession(temboo_account, temboo_app_name, temboo_app_key);
		} catch (TembooException e1) {
			error = true;
			errorString = "[   FAIL  ]  The Temboo session could not be created";
		}
		
		int no_fail=0, no_success=0;
		
		// will store failed operations so that they could be written back to the file
		String remainingOperations = "";
		
		// Get the operation list
		String operationsString=null;
		operationsString = dropboxGetFileContentsAsString(session, dropbox_list_file);
		showError(true); // throw an error and exit execution in case the file was not read/found
		String [] operations = operationsString.split("\n");
		
		System.out.println("Executing file operations: ");
		
		// Move the files as indicated in the file. Loop for each line
		for (int i=0; i<operations.length; i++) {
			
			// get the filenames indicated on each line
			String[] filesArray = operations[i].split(" ", 2);
			
			
			if (filesArray.length == 2) {
				
				// get the absolute paths for source and destination file
				String srcFile = app_src_dir + filesArray[0].trim();
				String dstFile = filesArray[1].trim();
				
				// attempt to move the file
				if (moveFileFromDropboxToLocal(session, srcFile, dstFile)) {
					System.out.println("[ SUCCESS ]  "+srcFile+" -> "+dstFile);
					no_success ++;
				}
				else { // in case the move fails for any reason
					errorString = "[   FAIL  ]  "+errorString+" "+srcFile+" -> "+dstFile;
					error=true;
					showError(false);
					remainingOperations += filesArray[0].trim() +" "+dstFile+"\n";
					no_fail ++;
				}
			} else { // in case the line from the file has an invalid structure
				// put the line back into the file and throw an error message, but continue execution
				errorString = "[   FAIL  ]  Line \""+operations[i].trim()+"\" is not valid.";
				error = true;
				showError(false);
				remainingOperations += operations[i].trim()+"\n";
				no_fail ++;
			}
		}
		
		System.out.println("\nWriting back the operations that failed...");
		
		// update the operation list in the file on Dropbox. 
		if(!dropboxDeleteFile(session, dropbox_list_file)) {
			errorString= "[   FAIL  ]  " + errorString;
			error = true;
			showError(true);
		}
		if(!dropboxWriteFile(session, dropbox_list_file, remainingOperations)) {
			errorString = "[   FAIL  ]  The file list could not be updated on Dropbox.";
			error = true;
			showError(true);
		} else
			System.out.println("[ SUCCESS ]  The operations that failed have been written back in "+dropbox_list_file);
		
		// final report
		System.out.println("\nSuccessful moves: " + no_success + "\nFailed moves: " + no_fail);
		
	}
	
	public static byte[] dropboxGetFileContentsAsByteArray(TembooSession session, String sourceFilename) {
		// this function gets the content of a Dropbox file in a byte array
		//
		// @session - Temboo session
		// @sourceFile - remote path of the file on Temboo
		
		// Open the file __list for reading the file movement operations
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set credential to use for execution
		getFileInputs.setCredential(temboo_app_credential);

		// Set inputs
		getFileInputs.set_Path(sourceFilename);

		// Execute Choreo
		GetFileResultSet getFileResults = null;
		try {
			getFileResults = getFileChoreo.execute(getFileInputs);
		} catch (TembooException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			error = true;
			errorString = "The Dropbox file ("+sourceFilename+") content could not be read or found.";
			return null;
		}
		
		// Get file content
		String responseBase64 = getFileResults.get_Response();
		byte[] responseByteArray = Base64.decodeBase64(responseBase64);
		
		return responseByteArray;
	}

	public static String dropboxGetFileContentsAsString(TembooSession session, String sourceFilename) {
		// this function gets the content of a Dropbox file in a String data structure
		//
		// @session - Temboo session
		// @sourceFile - remote path of the file on Temboo
		
		byte[] responseByteArray = dropboxGetFileContentsAsByteArray(session, sourceFilename);
		
		if (responseByteArray == null) return null;
		
		String responseString = new String(responseByteArray);

		return responseString;
	}
	
	
	public static boolean dropboxWriteFile(TembooSession session, String filename, String contents) {
		// this function writes a file from Dropbox.
		//
		// @session - Temboo session 
		// @filename - remote path of the file on Dropbox
		// @contents - the content of the file as String
		
		// strip the path of the first slash, if it has it
		if(filename.substring(0, 1).equals("/")) 
			filename = filename.substring(1);
		
		UploadFile uploadFileChoreo = new UploadFile(session);
	
		//encode the content
		byte [] contentBytes = Base64.encodeBase64(contents.getBytes());
		contents = new String(contentBytes);
		
		// Get an InputSet object for the choreo
		com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileInputSet uploadFileInputs = uploadFileChoreo.newInputSet();
	
		// Set credential to use for execution
		uploadFileInputs.setCredential(temboo_app_credential);
	
		// Set inputs
		if (filename.lastIndexOf("/") >=0) {
			uploadFileInputs.set_Folder(filename.substring(0, filename.lastIndexOf("/")+1));
			uploadFileInputs.set_FileName(filename.substring(filename.lastIndexOf("/")+1));
		} else uploadFileInputs.set_FileName(filename);
		
		//write the contents in the file
		uploadFileInputs.set_FileContents(contents);
	
		// Execute Choreo
		try {
			uploadFileChoreo.execute(uploadFileInputs);
		} catch (TembooException e) {
			error = true;
			errorString = "File ("+filename+") could not be written on Dropbox.";
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean dropboxDeleteFile(TembooSession session, String filename) {
		// this function deletes a specified file from Dropbox.
		//
		// @session - Temboo session 
		// @filename - remote path of the file on Dropbox
		
		
		DeleteFileOrFolder deleteFileOrFolderChoreo = new DeleteFileOrFolder(session);
	
		// Get an InputSet object for the choreo
		DeleteFileOrFolderInputSet deleteFileOrFolderInputs = deleteFileOrFolderChoreo.newInputSet();
	
		// Set inputs
		deleteFileOrFolderInputs.setCredential(temboo_app_credential);
		deleteFileOrFolderInputs.set_Path(filename);
	
		// Execute Choreo
		try {
			deleteFileOrFolderChoreo.execute(deleteFileOrFolderInputs);
		} catch (TembooException e) {
			error = true;
			errorString = "ERROR: The file "+filename+" could not be deleted.";
			return false;
		}
		
		return true;
	}

	public static boolean writeToLocalFile(String path, byte[] content) {
		// This function writes a byte array to a given local path
		//
		// @path - path of the local file
		// @content - the byte array of the data that needs to be written
		
		File file = new File(path);
		 
		// if file doesnt exists, then create it
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				error = true;
				errorString = "The file "+path+" could not be created.";
				return false;
			}
		} else {
			// error handling if there is a file already at the given path
			error = true;
			errorString = "The given local path("+path+") points to an existent file.";
			return false;
		}

		// write the content to the file
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			fos.write(content);
			fos.close();
			return true;
		} catch (FileNotFoundException e) {
			// error handled above. There is no scenario that could make this exception occur
		} catch (IOException e) {
			error = true;
			errorString = "No content could be written in the file " + path + ".";
			return false;
		}

		return true;
	}
	
	public static boolean moveFileFromDropboxToLocal(TembooSession session, String srcFile, String dstFile) {
		// This function moves a given file from Dropbox to a local path.
		// 
		// @session - Temboo session 
		// @srcFile - remote path of the file of Temboo
		// @dstFile - local path to which the file need to be moved
		
		byte[] content = null;
		
		content = dropboxGetFileContentsAsByteArray(session, srcFile);
		if (error) return false;
		
		// write the file locally. The function returns true if success.
		if(!writeToLocalFile(dstFile, content)) {
			// error description is handles in writeToLocalFile()
			return false;
		} else return true;
	}
	
	public static void showError(boolean terminate) {
		// this function shows an error message on the screen
		//
		// @terminate - if set to true, the program will exit
		
		if(error) {
			System.out.println(errorString);
			if (terminate)
				System.exit(0);
			error=false;
			errorString=null;
		}
	}
}